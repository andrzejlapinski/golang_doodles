package main

import "fmt"

func main() {
	var a bool
	defer fmt.Println(a==true)
	a=!a
}
